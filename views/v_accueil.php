<?php require_once(PATH_VIEWS.'header.php');?>

<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>

<!--  Début de la page -->
<h1><?= TITRE ?></h1>

<?php if (isset($error)) {
    echo "<h1>$error<h1>";
} ?>

<form action="index.php?page=accueil" method="get">
    <select name="numCat">
        <option value="0" selected>Pas de Filtre</option>
        <option value="1">Animaux</option>
        <option value="2">Bouffe</option>
        <option value="3">Batiment</option>
    </select>
    <input type="submit" value="Valider">
</form>

<?php
foreach ($res as $img) {
    echo '<img src='.PATH_IMAGES.$img['nomFich'].
    ' alt='.$img['description'].'>';
}
?>
<!--  Fin de la page -->

<!--  Pied de page -->
<?php require_once(PATH_VIEWS.'footer.php'); 
