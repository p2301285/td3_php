<?php
/*
 * Copyright 2016, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * menu: http://www.w3schools.com/bootstrap/bootstrap_ref_comp_navs.asp
 */
?>
<!-- Menu du site -->

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
		<li <?php echo ($page=='accueil' ? 'class="active"':'')?>>
			<a href="index.php">
				<?= MENU_ACCUEIL ?>
			</a>
		</li>
    </ul>

	<ul class="nav navbar-nav navbar-right">
		<?php
			if (isset($_SESSION['logged'])&&$_SESSION['logged']==true) {
		?>
			<li <?php echo ($page=='login' ? 'class="active"':'')?>>
				<a href="index.php?page=login">
					<p>Deconnexion</p>
				</a>
			</li>

			<li> 
			<a href="index.php">
					<p>Ajouter une image</p>
				</a>
			</li>
				<?php
			}else {
				?>
			<li <?php echo ($page=='login' ? 'class="active"':'')?>>
				<a href="index.php?page=login">
					<p>Connexion</p>
				</a>
			</li>
		<?php
			}
		?>
	</ul>
  </div>
</nav>


