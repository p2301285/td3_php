<?php require_once(PATH_VIEWS.'header.php');?>

<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>

<!--  Début de la page -->
<h1><?= TITRE ?></h1>

<?php 
    if(isset($error)){
        echo "<h2 color='red'>$error</h2>";
    }
    if (isset($_SESSION['logged'])) {
        if($_SESSION['logged']==true){
            echo 'connecté';
        }else{
            echo 'déconnecté';
        }
    }
?>

<form action="index.php" method="get">
    <input type="hidden" name="page" value="login">
    <input type="text" name="id" required>
    <input type="password" name="mdp" value="p4ss4dm1n" required>
    <input type="submit" value="Valider">
</form>
<!--  Fin de la page -->

<!--  Pied de page -->
<?php require_once(PATH_VIEWS.'footer.php'); 
