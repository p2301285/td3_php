<?php
 
const DEBUG = true; // production : false; dev : true

// Accès base de données
const BD_HOST = 'localhost';
const BD_DBNAME = 'p2301285';
const BD_USER = 'p2301285';
const BD_PWD = '719604';

// Langue du site
const LANG ='FR-fr';

//Utilisateur autorisé
define('GRANTED_LOGIN','admin');
define('PWD','$2y$10$muAQ1nQhENfbY3XktcraBOrCSSvEZbdllG/XYpAHtByZaBHHIiFCu');

// Paramètres du site : nom de l'auteur ou des auteurs
const AUTEUR = ''; 

//dossiers racines du site
define('PATH_CONTROLLERS','./controllers/c_');
define('PATH_ENTITY','./entities/');
define('PATH_ASSETS','./assets/');
define('PATH_LIB','./lib/');
define('PATH_MODELS','./models/');
define('PATH_VIEWS','./views/v_');
define('PATH_TEXTES','./languages/');

//sous dossiers
define('PATH_CSS', PATH_ASSETS.'css/');
define('PATH_IMAGES', PATH_ASSETS.'images/');
define('PATH_SCRIPTS', PATH_ASSETS.'scripts/');

//fichiers
define('PATH_LOGO', PATH_IMAGES.'logo.png');
define('PATH_MENU', PATH_VIEWS.'menu.php');
