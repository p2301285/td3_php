<?php
require_once(PATH_MODELS.'DAO.php');

class AccueilDAO extends DAO {
    public function getAllImgs() {
        $req = $this->queryAll('SELECT * FROM Photo');
        if($req){
            return $req;
        }
    }

    public function getById($id) {
        $req = $this->queryAll("SELECT * FROM Photo WHERE catId = ? ",[strval($id)]);
        if($req){
            return $req;
        }else {
            return null;
        }
    }
}